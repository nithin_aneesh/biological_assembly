/*
 * generate_biological_assembly.cpp
 *
 * Copyright 2022 Nithin Chandran
 * Assistant Professor (adiunkt)
 * Laboratory of Computational Biology
 * Faculty of Chemistry
 * Biological and Chemical Research Centre
 * University of Warsaw
 * Zwirki i Wigury 101
 * 02-089 Warsaw, Poland
 *
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include "generate_biological_assembly.h"

int main(int argc, char** argv)
{
	std::ifstream infile;
	std::string outfile_name;
	switch(argc)
	{
	case 2:
		infile.open(argv[1]);
		outfile_name = std::string(argv[1]) + "_bio_assembly";
		break;
	case 3:
		infile.open(argv[1]);
		outfile_name = argv[2];
		break;
	default:
		std::cerr << "Incorrect Useage:" << std::endl;
		std::cerr << "Correct useage is: " << argv[0]
				  << " PDB_input_file [PDB_output_file]" << std::endl;
		return EXIT_FAILURE;
	}
	std::stringstream PDB_lines;
	if(infile.is_open())
	{
		PDB_lines << infile.rdbuf();
		infile.close();
	}
	else
	{
		std::cerr << "Unable to open " << argv[1] << std::endl;
		return EXIT_FAILURE;
	}
	biomt_matrices biomt_matrices, smtry_matrices;
	auto biomt_exists = get_biomt_matrices(PDB_lines, biomt_matrices);
	if(!biomt_exists)
	{
		std::ofstream outfile;
		outfile.open(outfile_name + "_1.pdb");
		outfile << PDB_lines.rdbuf();
		outfile.close();
		std::cerr << "Remark 350 with BIOMT matrices are not available in the "
					 "PDB file"
				  << std::endl;
		std::cerr << "The contents of input are copied to " << outfile_name
				  << "_1.pdb without changes" << std::endl;
		return EXIT_FAILURE;
	}
	/* auto smtry_exists = get_smtry_matrices(PDB_lines, smtry_matrices); */
	generate_assemby(PDB_lines, biomt_matrices, outfile_name);
	return 0;
}

bool get_biomt_matrices(std::stringstream& PDB_lines,
						biomt_matrices& biomt_matrices)
{
	std::string line;
	bool biomt_exists = false;
	int assembly_id = 0;
	std::set<std::string> assembly_chains;
	while(std::getline(PDB_lines, line))
	{
		if(line != "")
		{
			if(line.starts_with("REMARK 350 BIOMOLECULE:"))
			{
				assembly_id = std::stoi(line.substr(23));
			}
			if(line.starts_with("REMARK 350 APPLY THE FOLLOWING TO CHAINS:") ||
			   line.starts_with("REMARK 350                    AND CHAINS:"))
			{
				if(line.starts_with(
					   "REMARK 350 APPLY THE FOLLOWING TO CHAINS:"))
				{
					assembly_chains.clear();
				}
				std::stringstream ss(line.substr(41));
				std::string token;
				while(std::getline(ss, token, ','))
				{
					token.erase(std::remove_if(token.begin(),
											   token.end(),
											   [](unsigned char x) {
												   return std::isspace(x);
											   }),
								token.end());
					assembly_chains.insert(token);
				}
			}
			if(line.starts_with("REMARK 350   BIOMT"))
			{
				biomt_exists = true;
				int matrix_line = std::stoi(line.substr(18, 1));
				int matrix_id = std::stoi(line.substr(19, 4));
				bool key_exists = biomt_matrices.contains(
					{assembly_id, assembly_chains, matrix_id});
				if(!key_exists)
				{
					biomt_matrices[{assembly_id, assembly_chains, matrix_id}] =
						{{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
						 {0.0, 0.0, 0.0}};
				}
				biomt_matrices[{assembly_id, assembly_chains, matrix_id}]
					.first[matrix_line - 1][0] = std::stod(line.substr(23, 10));
				biomt_matrices[{assembly_id, assembly_chains, matrix_id}]
					.first[matrix_line - 1][1] = std::stod(line.substr(33, 10));
				biomt_matrices[{assembly_id, assembly_chains, matrix_id}]
					.first[matrix_line - 1][2] = std::stod(line.substr(43, 10));
				biomt_matrices[{assembly_id, assembly_chains, matrix_id}]
					.second[matrix_line - 1] = std::stod(line.substr(53, 15));
			}
		}
	}
	PDB_lines.clear();
	PDB_lines.seekg(0);
	return biomt_exists;
}

/* bool get_smtry_matrices(std::stringstream& PDB_lines,
						biomt_matrices& smtry_matrices)
{
	std::string line;
	bool smtry_exists = false;
	while(std::getline(PDB_lines, line))
	{
		if(line != "" && line.starts_with("REMARK 290"))
		{
			if(line.starts_with("REMARK 290   SMTRY"))
			{
				smtry_exists = true;
				int matrix_line = std::stoi(line.substr(18, 1));
				int matrix_id = std::stoi(line.substr(19, 4));
				bool key_exists = smtry_matrices.contains(matrix_id);
				if(!key_exists)
				{
					smtry_matrices[matrix_id] = {
						{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
						{0.0, 0.0, 0.0}};
				}
				smtry_matrices[matrix_id].first[matrix_line - 1][0] =
					std::stod(line.substr(23, 10));
				smtry_matrices[matrix_id].first[matrix_line - 1][1] =
					std::stod(line.substr(33, 10));
				smtry_matrices[matrix_id].first[matrix_line - 1][2] =
					std::stod(line.substr(43, 10));
				smtry_matrices[matrix_id].second[matrix_line - 1] =
					std::stod(line.substr(53, 15));
			}
		}
	}
	PDB_lines.clear();
	PDB_lines.seekg(0);
	return smtry_exists;
} */

std::set<std::string> get_existing_chains(std::stringstream& PDB_lines)
{
	std::string line;
	std::set<std::string> list_of_chains;
	while(std::getline(PDB_lines, line))
		if(line != "" && (line.starts_with("ATOM")) ||
		   line.starts_with("HETATM"))
		{
			list_of_chains.insert(line.substr(21, 1));
		}
	PDB_lines.clear();
	PDB_lines.seekg(0);
	return list_of_chains;
}

int get_number_of_assemblies(std::stringstream& PDB_lines)
{
	std::string line;
	int number_of_assemblies = 1;
	while(std::getline(PDB_lines, line))
	{
		if(line != "" && line.starts_with("REMARK 300 BIOMOLECULE:"))
		{
			std::stringstream ss(line.substr(23));
			std::string token;
			while(std::getline(ss, token, ','))
			{
				number_of_assemblies = std::stoi(token);
			}
		}
	}
	PDB_lines.clear();
	PDB_lines.seekg(0);
	return number_of_assemblies;
}

std::map<int, std::set<std::string>>
get_assembly_chain_pairs(std::stringstream& PDB_lines)
{
	std::map<int, std::set<std::string>> assembly_chains;
	std::string line;
	int assembly_id = 0;
	while(std::getline(PDB_lines, line))
	{
		if(line != "")
		{
			if(line.starts_with("REMARK 350 BIOMOLECULE:"))
				assembly_id = std::stoi(line.substr(23));
			if(line.starts_with("REMARK 350 APPLY THE FOLLOWING TO CHAINS:") ||
			   line.starts_with("REMARK 350                    AND CHAINS:"))
			{
				std::stringstream ss(line.substr(41));
				std::string token;
				while(std::getline(ss, token, ','))
				{
					token.erase(std::remove_if(token.begin(),
											   token.end(),
											   [](unsigned char x) {
												   return std::isspace(x);
											   }),
								token.end());
					(assembly_chains[assembly_id]).insert(token);
				}
			}
		}
	}
	PDB_lines.clear();
	PDB_lines.seekg(0);
	for(auto [i, j] : assembly_chains)
	{
		std::cerr << "Assembly ID " << i << std::endl;
		for(auto k : j)
			std::cerr << k << ", ";
		std::cerr << std::endl;
	}
	return assembly_chains;
}

bool generate_assemby(std::stringstream& PDB_lines,
					  biomt_matrices& biomt_matrices,
					  std::string outfile_name)
{
	auto list_of_chains = get_existing_chains(PDB_lines);
	std::string line;
	int number_of_assemblies = get_number_of_assemblies(PDB_lines);
	std::stringstream ss[number_of_assemblies], header[number_of_assemblies],
		seqres_records_original, seqres_records_assembly[number_of_assemblies],
		ss_records_original, ss_records_assembly[number_of_assemblies];
	auto seqres_exists(get_seqres_records(PDB_lines, seqres_records_original));
	auto ss_exists(get_ss_records(PDB_lines, ss_records_original));
	biomt_matrix unit_matrix = {{1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0},
								{0.0, 0.0, 0.0}};
	int previous_assembly_id = 0;
	std::vector<int> atom_number(number_of_assemblies, 0);
	std::set<std::string> alphabetical_chains, numerical_chains, remaining_chains;
	
	for(auto const& [t, matrices] : biomt_matrices)
	{
		auto assembly_id = std::get<0>(t);
		auto assembly_chains = std::get<1>(t);
		auto matrix_id = std::get<2>(t);
		auto rotation = matrices.first;
		auto translation = matrices.second;
		if(previous_assembly_id != assembly_id)
		reset_available_chains(assembly_chains, alphabetical_chains, numerical_chains, remaining_chains);
		previous_assembly_id = assembly_id;
		std::cerr << "Matrix " << matrix_id << " for biological assembly "
				  << assembly_id << std::endl;
		std::cerr << "Rotation Matrix " << std::endl;
		for(auto k : rotation)
		{
			for(auto l : k)
			{
				std::cerr << l << "\t";
			}
			std::cerr << std::endl;
		}
		std::cerr << "Translation Matrix " << std::endl;
		for(auto m : translation)
		{
			std::cerr << m << "\t";
		}
		std::cerr << std::endl;

		for(auto i : assembly_chains)
		{
			if(matrices == unit_matrix)
			{
				std::cerr << "This matrix is not applied to chain " << i
						  << " as it is a unit matrix" << std::endl;
				while(std::getline(PDB_lines, line))
				{
					if(line != "")
					{
						if(line.starts_with("MODEL       ") ||
						   line.starts_with("ENDMDL"))
							ss[assembly_id - 1] << line << std::endl;
						if((line.starts_with("ATOM") ||
							line.starts_with("HETATM")) &&
						   line.substr(21, 1) == i)
						{
							atom_number[assembly_id - 1]++;
							line.replace(6,
										 5,
										 get_fixed_length_string_from_int(
											 atom_number[assembly_id - 1], 5));
							ss[assembly_id - 1] << line << std::endl;
						}
					}
				}
				PDB_lines.clear();
				PDB_lines.seekg(0);
				if(seqres_exists)
					generate_seqres_record(
						seqres_records_original,
						seqres_records_assembly[assembly_id - 1],
						i,
						i);
				if(ss_exists)
					generate_ss_record(ss_records_original,
									   ss_records_assembly[assembly_id - 1],
									   i,
									   i);
				continue;
			}
			if(remaining_chains.empty())
			{
				if(numerical_chains.empty())
				{
					std::cerr << "The number of chains is greater than 62 "
								 "which is not supported"
							  << std::endl;
					return EXIT_FAILURE;
				}
				remaining_chains = numerical_chains;
				numerical_chains.clear();
			}
			auto first(remaining_chains.begin());
			std::string chain_ID = *first;
			remaining_chains.erase(chain_ID);
			std::cerr << "This matrix is applied to chain " << i
					  << " to generate chain " << chain_ID << std::endl;
			while(std::getline(PDB_lines, line))
			{
				if(line != "")
				{
					if(line.starts_with("MODEL       ") ||
					   line.starts_with("ENDMDL"))
						ss[assembly_id - 1] << line << std::endl;
					if((line.starts_with("ATOM") ||
						line.starts_with("HETATM")) &&
					   line.substr(21, 1) == i)
					{
						double x, y, z, xt, yt, zt;
						x = std::stod(line.substr(30, 8));
						y = std::stod(line.substr(38, 8));
						z = std::stod(line.substr(46, 8));
						xt = (x * rotation[0][0] + y * rotation[0][1] +
							  z * rotation[0][2]) +
							 translation[0];
						yt = (x * rotation[1][0] + y * rotation[1][1] +
							  z * rotation[1][2]) +
							 translation[1];
						zt = (x * rotation[2][0] + y * rotation[2][1] +
							  z * rotation[2][2]) +
							 translation[2];
						line.replace(21, 1, chain_ID);
						atom_number[assembly_id - 1]++;
						line.replace(6,
									 5,
									 get_fixed_length_string_from_int(
										 atom_number[assembly_id - 1], 5));
						ss[assembly_id - 1] << line.substr(0, 30);
						ss[assembly_id - 1] << std::setprecision(3)
											<< std::setw(8) << std::fixed << xt;
						ss[assembly_id - 1] << std::setprecision(3)
											<< std::setw(8) << std::fixed << yt;
						ss[assembly_id - 1] << std::setprecision(3)
											<< std::setw(8) << std::fixed << zt;
						ss[assembly_id - 1] << line.substr(54) << std::endl;
					}
				}
			}
			PDB_lines.clear();
			PDB_lines.seekg(0);
			if(seqres_exists)
				generate_seqres_record(seqres_records_original,
									   seqres_records_assembly[assembly_id - 1],
									   i,
									   chain_ID);
			if(ss_exists)
				generate_ss_record(ss_records_original,
								   ss_records_assembly[assembly_id - 1],
								   i,
								   chain_ID);
		}
	}

	for(size_t i = 0; i < number_of_assemblies; ++i)
	{
		std::ofstream outfile;
		bool add_seqres = false, add_ss = false;
		outfile.open(outfile_name + "_" + std::to_string(i + 1) + ".pdb");
		while(std::getline(PDB_lines, line))
		{
			if(!(line.starts_with("ATOM") || line.starts_with("HETATM") ||
				 line.starts_with("ANISOU") || line.starts_with("MASTER") ||
				 line.starts_with("MODEL       ") || line.starts_with("TER") ||
				 line.starts_with("END") || line.starts_with("CONECT")))
			{
				if(line.starts_with("SEQRES"))
				{
					add_seqres = true;
					continue;
				}
				else if(add_seqres)
				{
					header[i] << seqres_records_assembly[i].str();
					add_seqres = false;
				}
				if(line.starts_with("HELIX") || line.starts_with("SHEET"))
				{
					add_ss = true;
					continue;
				}
				else if(add_ss)
				{
					header[i] << ss_records_assembly[i].str();
					add_ss = false;
				}
				header[i] << line << std::endl;
			}
		}
		PDB_lines.clear();
		PDB_lines.seekg(0);
		outfile << header[i].rdbuf() << ss[i].rdbuf();
		outfile.close();
	}
	return true;
}

std::string get_fixed_length_string_from_int(int a, size_t length)
{
	std::string s = std::to_string(a);
	while(s.length() < length)
		s = " " + s;
	return s;
}

bool get_seqres_records(std::stringstream& PDB_lines, std::stringstream& ss)
{
	std::string line;
	bool seqres_exists = false;
	while(std::getline(PDB_lines, line))
	{
		if(line.starts_with("SEQRES"))
		{
			seqres_exists = true;
			ss << line << std::endl;
		}
	}
	PDB_lines.clear();
	PDB_lines.seekg(0);
	return seqres_exists;
}

bool generate_seqres_record(std::stringstream& seqres_records_original,
							std::stringstream& seqres_records_assembly,
							std::string orig_chain,
							std::string new_chain)
{
	std::cerr << "Generating SEQRES for " << new_chain << std::endl;
	std::string line;
	bool seqres_modified = false;
	while(std::getline(seqres_records_original, line))
	{
		if(line != "" && line.starts_with("SEQRES"))
		{
			if(line.substr(11, 1) == orig_chain)
			{
				if(orig_chain != new_chain)
				{
					line.replace(11, 1, new_chain);
					seqres_modified = true;
				}
				seqres_records_assembly << line << std::endl;
				std::cerr << line << std::endl;
			}
		}
	}
	seqres_records_original.clear();
	seqres_records_original.seekg(0);
	return seqres_modified;
}

bool get_ss_records(std::stringstream& PDB_lines, std::stringstream& ss)
{
	std::string line;
	bool ss_exists = false;
	while(std::getline(PDB_lines, line))
	{
		if(line.starts_with("HELIX") || line.starts_with("SHEET"))
		{
			ss_exists = true;
			ss << line << std::endl;
		}
	}
	PDB_lines.clear();
	PDB_lines.seekg(0);
	return ss_exists;
}

bool generate_ss_record(std::stringstream& ss_records_original,
						std::stringstream& ss_records_assembly,
						std::string orig_chain,
						std::string new_chain)
{
	std::cerr << "Generating Secondary Structure for " << new_chain
			  << std::endl;
	std::string line;
	bool ss_modified = false;
	while(std::getline(ss_records_original, line))
	{
		if(line != "")
		{
			if(line.starts_with("HELIX"))
			{
				if(line.substr(19, 1) == orig_chain)
				{
					if(orig_chain != new_chain)
					{
						line.replace(19, 1, new_chain);
						line.replace(31, 1, new_chain);
						ss_modified = true;
					}
					ss_records_assembly << line << std::endl;
					std::cerr << line << std::endl;
				}
			}
			if(line.starts_with("SHEET"))
			{
				if(line.substr(21, 1) == orig_chain)
				{
					if(orig_chain != new_chain)
					{
						line.replace(21, 1, new_chain);
						line.replace(32, 1, new_chain);
						if(line.length() >= 65)
						{
							line.replace(49, 1, new_chain);
							line.replace(64, 1, new_chain);
						}
						ss_modified = true;
					}
					ss_records_assembly << line << std::endl;
					std::cerr << line << std::endl;
				}
			}
		}
	}
	ss_records_original.clear();
	ss_records_original.seekg(0);
	return ss_modified;
}

bool reset_available_chains(std::set<std::string>& assembly_chains, std::set<std::string>& alphabetical_chains, std::set<std::string>& numerical_chains, std::set<std::string>& remaining_chains)
{
	alphabetical_chains = {
		"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M",
		"N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z",
		"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m",
		"n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"};
	numerical_chains = {
		"1", "2", "3", "4", "5", "6", "7", "8", "9", "0"};
	std::set_difference(
		alphabetical_chains.begin(),
		alphabetical_chains.end(),
		assembly_chains.begin(),
		assembly_chains.end(),
		std::inserter(remaining_chains, remaining_chains.begin()));
	return true;
}