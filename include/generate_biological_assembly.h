/*
 * generate_biological_assembly.h
 *
 * Copyright 2022 Nithin Chandran
 * Assistant Professor (adiunkt)
 * Laboratory of Computational Biology
 * Faculty of Chemistry
 * Biological and Chemical Research Centre
 * University of Warsaw
 * Zwirki i Wigury 101
 * 02-089 Warsaw, Poland
 *
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 *
 *
 */

#include <algorithm>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <map>
#include <set>
#include <sstream>
#include <string>
#include <vector>

using rotation_matrix = std::array<std::array<double, 3>, 3>;
using translation_matrix = std::array<double, 3>;
using biomt_matrix = std::pair<rotation_matrix, translation_matrix>;
using xyz_data = std::vector<std::array<double, 3>>;
using biomt_matrices =
	std::map<std::tuple<int, std::set<std::string>, int>, biomt_matrix>;

bool get_biomt_matrices(std::stringstream& PDB_lines,
						biomt_matrices& biomt_matrices);

/* bool get_smtry_matrices(std::stringstream& PDB_lines,
						biomt_matrices& smtry_matrices); */

int get_number_of_assemblies(std::stringstream& PDB_lines);

std::map<int, std::set<std::string>>
get_assembly_chain_pairs(std::stringstream& PDB_lines);

bool get_seqres_records(std::stringstream& PDB_lines, std::stringstream& ss);

bool get_ss_records(std::stringstream& PDB_lines, std::stringstream& ss);

std::set<std::string> get_existing_chains(std::stringstream& PDB_lines);

bool generate_assemby(std::stringstream& PDB_lines,
					  biomt_matrices& biomt_matrices,
					  std::string outfile_name);

std::string get_fixed_length_string_from_int(int a, size_t length);

bool generate_seqres_record(std::stringstream& seqres_records_original,
							std::stringstream& seqres_records_assembly,
							std::string orig_chain,
							std::string new_chain);

bool generate_ss_record(std::stringstream& ss_records_original,
						std::stringstream& ss_records_assembly,
						std::string orig_chain,
						std::string new_chain);

bool reset_available_chains(std::set<std::string>& assembly_chains, std::set<std::string>& chains, std::set<std::string>& extended_chains, std::set<std::string>& remaining_chains);