CXX = g++
CXXFLAGS = --std=c++23 -w -O3 -Iinclude
LDFLAGS_P = -pthread
LDFLAGS_S = -DSEQUENTIAL

SOURCES = src/generate_biological_assembly.cpp

OBJECTS = $(SOURCES:.cpp=.o)


TARGET = bioassembly

default:
	$(CXX) $(CXXFLAGS) $(LDFLAGS_S) -o $(TARGET) $(SOURCES)

clean:
	rm -f $(OBJECTS) $(TARGET)
